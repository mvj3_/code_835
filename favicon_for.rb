  def favicon_for(url)
    matches = url.match(/[^:\/]\/(.*)/)
    image_tag url.sub(matches[1], '') + '/favicon.ico', {width: '16px', height: '16px'}
  end


Pismo::Document.new('http://www.facebook.com/').favicon